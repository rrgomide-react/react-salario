import React, { Component, Fragment } from 'react';

import './App.css';
import * as reactLogo from './assets/react.png';

import { interval } from 'rxjs';
import { takeUntil, map, filter } from 'rxjs/operators';

import { TempoReal } from './components/TempoReal';
import { CalculoReverso } from './components/CalculoReverso';
import { Salario } from './classes/Salario';

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      salario: new Salario(0),
      salarioLiquidoDesejado: 5000,
      calculandoSalario: false
    };

    this.salarioChange = this.salarioChange.bind(this);
    this.calculoReverso = this.calculoReverso.bind(this);
    this.salarioLiquidoDesejadoChange = this.salarioLiquidoDesejadoChange.bind(
      this
    );
  }

  salarioChange(event) {
    this.setState({ salario: new Salario(+event.target.value) });
  }

  salarioLiquidoDesejadoChange(event) {
    this.setState({ salarioLiquidoDesejado: +event.target.value });
  }

  calculoReverso() {
    this.setState({
      calculandoSalario: true,
      salario: new Salario(this.state.salarioLiquidoDesejado)
    });

    const inputSalarioBruto = document.querySelector('#inputSalarioBruto');

    /**
     * Criando observable que, a cada 1 milisegundo, incrementa
     * o salário bruto e o recalcula no estado da aplicação. Por
     * fim, retorna o salarioLiquido obtido após o cálculo.
     *
     */
    const obs$ = interval(1).pipe(
      /**
       * Transformação de dados (map)
       */
      map(() => {
        /**
         * Obtendo o salário líquido do momento
         */
        const currentValue = +this.state.salario.salarioLiquido;

        /**
         * Calculando a diferença entre o salário líquido do momento
         * e o salário líquido desejado
         */
        const difference = Math.abs(
          currentValue - this.state.salarioLiquidoDesejado
        );

        /**
         * Quando a diferença for menor que 5 reais, o
         * incremento passa a ser de 1 centavo (0.01)
         * para uma melhor precisão no cálculo sem que
         * o mesmo se torne lento, ou seja, enquanto a
         * diferença for maior que 5 reais o incremento
         * é de "1 em 1 real"
         */
        const increment = difference >= 5 ? 1 : 0.01;

        /**
         * Incrementando o valor no salário bruto
         * e formatando para 2 casas decimais
         */
        const novoSalario = +this.state.salario.salarioBruto + increment;

        inputSalarioBruto.value = novoSalario.toFixed(2);

        /**
         * Atualizando o salário bruto. Quando atualizamos o valor
         * "na mão", o Vue não consegue monitorar as
         * mudanças
         */
        this.setState({
          salario: new Salario(novoSalario)
        });

        /**
         * Por fim, retornamos o salário líquido atual
         */
        return this.state.salario.salarioLiquido;
      })
    );

    /**
     * Observable para ser utilizado com takeUntil,
     * que será a condição de término da execução
     * (salarioLiquido do estado maior ou igual ao salarioLiquido desejado)
     */
    const match$ = obs$.pipe(
      filter(currentValue => +currentValue >= this.state.salarioLiquidoDesejado)
    );

    /**
     * Acionamos, por fim, a execução do observable com
     * subscribe()
     */
    obs$.pipe(takeUntil(match$)).subscribe(null, null, () =>
      this.setState({
        calculandoSalario: false
      })
    );
  }

  render() {
    return (
      <Fragment>
        <div className="row">
          <img src={reactLogo} width="100px" alt="React" />
          <h3>Cálculo de salário com React</h3>
        </div>

        <div className="content">
          <div className="mainContent">
            <TempoReal
              salario={this.state.salario}
              onChange={this.salarioChange}
              calculandoSalario={this.state.calculandoSalario}
            />
          </div>
          <div className="sideContent">
            <CalculoReverso
              onCalculo={this.calculoReverso}
              onChange={this.salarioLiquidoDesejadoChange}
              calculandoSalario={this.state.calculandoSalario}
            />
          </div>
        </div>
      </Fragment>
    );
  }
}
