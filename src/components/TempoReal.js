import React, { Fragment } from 'react';
import { LabeledInput } from './LabeledInput';

export const TempoReal = props => {
  const {
    baseINSS,
    descontoINSS,
    baseIRPF,
    descontoIRPF,
    salarioLiquido
  } = props.salario;

  return (
    <Fragment>
      <LabeledInput
        label="Salário bruto:"
        onChange={props.onChange}
        customId="inputSalarioBruto"
      />

      <LabeledInput disabled currency label="Base INSS:" value={baseINSS} />

      <LabeledInput
        disabled
        currency
        label="Desconto INSS:"
        value={descontoINSS}
      />

      <LabeledInput disabled currency label="Base IRPF:" value={baseIRPF} />

      <LabeledInput
        disabled
        currency
        label="Desconto IRPF:"
        value={descontoIRPF}
      />

      <LabeledInput
        disabled
        currency
        label="Salário líquido:"
        value={salarioLiquido}
      />
    </Fragment>
  );
};
