import React, { Fragment } from 'react';
import { LabeledInput } from './LabeledInput';

export const CalculoReverso = props => (
  <Fragment>
    <h5>Cálculo reverso com Observables</h5>

    <LabeledInput
      label="Salário líquido desejado:"
      customId="inputSalarioLiquidoDesejado"
      value="5000"
      onChange={props.onChange}
    />

    <button className="btn" onClick={props.onCalculo}>
      Calcular salário bruto correspondente
    </button>
  </Fragment>
);
