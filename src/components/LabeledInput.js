import React, { Fragment } from 'react';

export const LabeledInput = props => (
  <Fragment>
    <label>
      {props.label}
      <input
        type={!!props.currency ? 'text' : 'number'}
        value={
          !!props.currency
            ? (+props.value).toLocaleString(
                'pt-BR',
                {
                  style: 'currency',
                  currency: 'BRL'
                }
              )
            : props.value
        }
        min="0"
        disabled={props.disabled}
        id={props.customId}
        onChange={props.onChange}
      />
    </label>
  </Fragment>
);
